import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components';

import CenaPrincipal from './src/components/CenaPrincipal';
import CenaClientes from './src/components/CenaClientes';
import CenaContatos from './src/components/CenaContatos';
import CenaAEmpresa from './src/components/CenaAEmpresa';
import CenaServicos from './src/components/CenaServicos';

export default class atmConsultoria extends Component {
  render() {
    return (
      <Navigator
        initialRoute={{ id: 'home' }}
        renderScene={(route, navigator) => {
          if(route.id === 'home') {
            return (<CenaPrincipal navigator={navigator}/>);
          }
          if(route.id === 'clientes') {
            return (<CenaClientes navigator={navigator}/>);
          }
          if(route.id === 'contatos') {
            return (<CenaContatos navigator={navigator}/>);
          }
          if(route.id === 'empresa') {
            return (<CenaAEmpresa navigator={navigator}/>);
          }
          if(route.id === 'servicos') {
            return (<CenaServicos navigator={navigator}/>);
          }
        }}
      />
    );
  }
}

AppRegistry.registerComponent('atmConsultoria', () => atmConsultoria);
