import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Image,
  Text,
  StyleSheet
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const detalheServicos = require('../../img/detalhe_servico.png');

export default class CenaServicos extends Component {
  render() {
    const { scope, text, resume, txtResume } = styles;

    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <StatusBar backgroundColor='#19D1C8'/>
        <BarraNavegacao navigator={this.props.navigator} back bgColor='#19D1C8'/>

        <View style={scope}>
          <Image source={detalheServicos}/>
          <Text style={text}>Nossos Serviços</Text>
        </View>

        <View style={resume}>
          <Text style={txtResume}>* Consultorias</Text>
          <Text style={txtResume}>* Propostas</Text>
          <Text style={txtResume}>* Acompanhamento de Processos</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scope: {
    flexDirection: 'row',
    marginLeft: 20,
    marginTop: 20
  },
  text: {
    fontSize: 30,
    alignSelf: 'center',
    marginLeft: 20,
    color: '#19D1C8'
  },
  resume: {
    padding: 20
  },
  txtResume: {
    fontSize: 18,
    marginBottom: 5
  }
});