import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Text,
  Image,
  StyleSheet
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const detalheEmpresa = require('../../img/detalhe_empresa.png');

export default class CenaAEmpresa extends Component {
  render() {
    const { scope, txt, scopeTxt, description } = styles;

    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <StatusBar backgroundColor='#EC7148'/>
        <BarraNavegacao navigator={this.props.navigator} back bgColor='#EC7148'/>

        <View style={scope}>
          <Image source={detalheEmpresa}/>
          <Text style={txt}>A Empresa</Text>
        </View>

        <View style={scopeTxt}>
          <Text style={description}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pharetra tortor massa, quis dapibus orci suscipit vel. Nulla lobortis vulputate elementum.</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scope: {
    flexDirection: 'row',
    marginLeft: 20,
    marginTop: 20
  },
  txt: {
    fontSize: 30,
    alignSelf: 'center',
    marginLeft: 15,
    color: '#EC7148'
  },
  scopeTxt: {
    padding: 20
  },
  description: {
    fontSize: 16,
    letterSpacing: 2
  }
});