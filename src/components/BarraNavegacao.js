import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableHighlight
} from 'react-native';

const btnBack = require('../../img/btn_voltar.png');

export default class BarraNavegacao extends Component {
  render() {
    const { barraTitulo, titulo } = styles;

    if(this.props.back) {
      return (
        <View style={[barraTitulo, {backgroundColor: this.props.bgColor}]}>
          <TouchableHighlight
            underlayColor={this.props.bgColor}
            activeOpacity={0.3}
            onPress={() => this.props.navigator.pop()}>
            <Image source={btnBack}/>
          </TouchableHighlight>
          <Text style={titulo}>ATM Consultoria</Text>
        </View>
      )
    }
    return (
      <View style={barraTitulo}>
        <Text style={titulo}>ATM Consultoria</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  barraTitulo: {
    backgroundColor: '#ccc',
    padding: 10,
    height: 60,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row'
  },
  titulo: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333'
  }
});