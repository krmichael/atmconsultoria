import React, { Component } from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  StyleSheet
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const detalheContato = require('../../img/detalhe_contato.png')

export default class CenaContatos extends Component {
  render() {
    const { detalhe, txtDetalhe, contato, txtContato } = styles;

    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <StatusBar backgroundColor='#61BD8C'/>
        <BarraNavegacao navigator={this.props.navigator} back bgColor='#61BD8C'/>

        <View style={detalhe}>
          <Image source={detalheContato}/>
          <Text style={txtDetalhe}>Contatos</Text>
        </View>

        <View style={contato}>
          <Text style={txtContato}>TEL: (34) 4004-4435</Text>
          <Text style={txtContato}>CEL: (61) 0000-0000</Text>
          <Text style={txtContato}>EMAIL: contatoatm@consul.com</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  detalhe: {
    flexDirection: 'row',
    marginLeft: 20
  },
  txtDetalhe: {
    alignSelf: 'center',
    fontSize: 30,
    color: '#61BD8C',
    marginLeft: 10
  },
  contato: {
    padding: 10,
    marginLeft: 20
  },
  txtContato: {
    fontSize: 16,
    marginBottom: 5
  }
});