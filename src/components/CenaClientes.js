import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Image,
  Text,
  StyleSheet
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const detalheCliente = require('../../img/detalhe_cliente.png');
const cliente1 = require('../../img/cliente1.png');
const cliente2 = require('../../img/cliente2.png');

export default class CenaClientes extends Component {
  render() {
    const { cabecalho, txtCabecalho, detalhe, txtDetalhe } = styles;

    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <StatusBar backgroundColor='#B9C941' />
        <BarraNavegacao navigator={this.props.navigator} back bgColor='#B9C941'/>

        <View style={cabecalho}>
          <Image source={detalheCliente} />
          <Text style={txtCabecalho}>Nossos clientes</Text>
        </View>

        <View style={detalhe}>
          <Image source={cliente1} />
          <Text style={txtDetalhe}>Dados do cliente 1</Text>
        </View>

        <View style={detalhe}>
          <Image source={cliente2} />
          <Text style={txtDetalhe}>Dados do cliente 2</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cabecalho: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 10
  },
  txtCabecalho: {
    fontSize: 30,
    color: '#B9C941',
    marginLeft: 10,
    marginTop: 25
  },
  detalhe: {
    padding: 20,
    marginTop: 10
  },
  txtDetalhe: {
    fontSize: 18,
    marginLeft: 20
  }
});