import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Image,
  StyleSheet,
  TouchableHighlight
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const logoApp = require('../../img/logo.png');
const menuCliente = require('../../img/menu_cliente.png');
const menuContato = require('../../img/menu_contato.png');
const menuEmpresa = require('../../img/menu_empresa.png');
const menuServico = require('../../img/menu_servico.png');

export default class CenaPrincipal extends Component {
  render() {
    const { logo, menu, menuItem, image } = styles;

    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <StatusBar backgroundColor='#ccc' />
        <BarraNavegacao />

        <View style={logo}>
          <Image source={logoApp} />
        </View>
        <View style={menu}>
          <View style={menuItem}>
            <TouchableHighlight
              underlayColor='#B9C941'
              activeOpacity={0.3}
              onPress={() => this.props.navigator.push({ id: 'clientes' })}>
              <Image style={image} source={menuCliente} />
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor='#61BD8C'
              activeOpacity={0.3}
              onPress={() => this.props.navigator.push({ id: 'contatos' })}>
              <Image style={image} source={menuContato} />
            </TouchableHighlight>
          </View>

          <View style={menuItem}>
            <TouchableHighlight
              underlayColor='#EC7148'
              activeOpacity={0.3}
              onPress={() => this.props.navigator.push({ id: 'empresa' })}>
              <Image style={image} source={menuEmpresa} />
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor='#19D1C8'
              activeOpacity={0.3}
              onPress={() => this.props.navigator.push({ id: 'servicos' })}>
              <Image style={image} source={menuServico} />
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    alignItems: 'center',
    marginTop: 30
  },
  menu: {
    alignItems: 'center'
  },
  menuItem: {
    flexDirection: 'row'
  },
  image: {
    margin: 15
  }
});